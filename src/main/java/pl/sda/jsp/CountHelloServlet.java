package pl.sda.jsp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
		name = "counthello",
		value = "/counthello",
		initParams = {
				@WebInitParam(name = "who", value = "World"),
				@WebInitParam(name = "times", value = "5")
		},
		loadOnStartup = -1  // dla -1 nie jest ladowany przy starcie, dla 0,1,2,itd jest wg zadanej kolejnosci; 0-najwczesniej
)
// pamietajcie, ze na serwerze utworzony jest 1 (slownie jeden) obiekt danego servletu
// kazde zadanie, to wywolanie metody doGet(), doPost() lub innej
// natomiast pola klasy who i times sa "wspoldzielone" przez wszystkie watki uruchamiające ww metody
// co to oznacza (w skrocie)? Jesli podczas jednego zadania zmienicie wartosc who lub times,
// to pozostale zadania beda juz widzialy ta zmiane (nie beda mialy dostepu do oryginalej wartosci).
// Do tego dochodzi oczywiscie wspolbiezny dostep, ale to inna historia :)
public class CountHelloServlet extends HttpServlet {

	private String who;
	private int times;

	@Override
	public void init() throws ServletException {
		// pobieramy parametry konfiguracyjne servletu
		// to NIE SA parametry z URLa
		// mozemy miec tak samo nazwane parametry konfiguracyjne oraz te z URLa,
		// ale beda to zupelnie inne parametry
		who = getInitParameter("who");
		times = Integer.parseInt(getInitParameter("times"));
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		PrintWriter out = resp.getWriter();

		// loopCount przechowuje informacje, ile razy zrobic obrot petla; przypisujemy jej wartosc domyslna z konfiguracji
		int loopCount = times;
		String name = who;

		// pobieramy z URLa parametr 'ilosc' - klient moze (przekazujac taki parametr) zmienic liczbe obrotow petli
		String iloscAsString = req.getParameter("ilosc");

		// sprawdzamy, czy ten parametr byl ustawiony
		// UWAGA. traktujcie parametry jak HashMape<String, String>
		// getParameter() to jakby wywolanie metody get() na HashMapie
		if (iloscAsString != null && !iloscAsString.isEmpty()) {
			// jesli nie jest null i nie jest pusty
			// !!!! (oraz zakladam, ze liczba)
			// to pobieram ta wartosc jako ilosc obrotow petli
			loopCount = Integer.valueOf(iloscAsString);
		}

		String nameAsString = req.getParameter("imie");

		if (nameAsString != null && !nameAsString.isEmpty()) {
			name = nameAsString;
		}

		// http://localhost:8080/counthello?who=Heniek&times=7

		for (int i = 0; i < loopCount; i++) {
			out.append("Hello, ").append(name).append("!\n");
		}

	}
}
