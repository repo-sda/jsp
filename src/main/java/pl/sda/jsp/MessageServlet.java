package pl.sda.jsp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "message", value = "/message.html")
public class MessageServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// logika "biznesowa"
		req.setAttribute("msg", "Wiadomość od servletu");

		// przekierowujemy na widok
		req.getRequestDispatcher("message.jsp").forward(req, resp);
	}
}
