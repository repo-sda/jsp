package pl.sda.jsp;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// konfiguracja jest w web.xml
public class SumServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		PrintWriter out = resp.getWriter();

		// np. http://localhost:8080/sum?a=6&b=88
		String a = req.getParameter("a");   // pobieramy parametr o nazwie 'a' z URL
		String b = req.getParameter("b");

		// mógł być niepodany lub pusty tj. a=
		if (a == null || a.isEmpty()) {
			out.println("Parametr 'a' jest null lub pusty!\n");
		}

		if (b == null || b.isEmpty()) {
			out.println("Parametr 'b' jest null lub pusty!\n");
		}

		// Uwaga! Tutaj nie walidujemy, czy w zmiennych a i b (w Stringu) sa TYLKO cyfry,
		// więc może poleciec wyjątek NumberFormatException
		// Tutaj macie kilka rozwiazan: https://stackoverflow.com/questions/1486077/good-way-to-encapsulate-integer-parseint
		Integer aAsInt = Integer.valueOf(a);
		Integer bAsInt = Integer.valueOf(b);

		out.append("a = " + aAsInt + "\n");
		out.append("b = " + bAsInt + "\n");
		out.append("a + b = " + (aAsInt + bAsInt) + "\n");

	}
}
