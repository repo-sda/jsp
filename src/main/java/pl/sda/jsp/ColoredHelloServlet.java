package pl.sda.jsp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ColoredHello", value = "/colored")
public class ColoredHelloServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();

		// ustawiamy nagłówek Content-Type na text/html - żeby przeglądarka traktowała otrzymane dane jako "strone internetowa"
		resp.setContentType("text/html");

		// definiujemy liste dopuszczalnych kolorow (jako cwiczenie, warto przeniesc ja do parametrow servletu)
		List<String> allowedColors = Arrays.asList("red", "green", "blue");

		// ustawiamy kolor czcionki na czarny
		String color = "black";

		// pobieramy parametr 'c', w którym klient mogl podac inny kolor, np. red, cyan, yellow, blue
		String c = req.getParameter("c");

		// jesli kolor jest podany
		if (c != null && !c.isEmpty()) {
			// sprawdzamy, czy kolor jest na liscie (prosciej bedzie uzyc contains)
			for (String allowedColor : allowedColors) {
				if (allowedColor.equals(c)) {
					// przepisujemy wartosc koloru podanego przez uzytkownika
					color = c;
					// jesli jest, to nie ma sensu dalej szukac
					break;
				}
			}
		}
		out.println("<span style='color: " + color + "'>Hello, World!</span>");
	}
}
