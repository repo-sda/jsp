package pl.sda.jsp.calc;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "calculatorServlet", value = "/calculator")
public class CalculatorServlet extends HttpServlet {

	private final int MIN = 0;
	private final int MAX = 100;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// ustawiamy typ kontentu jako strona internetowa
		resp.setContentType("text/html;charset=UTF-8");
		//resp.setCharacterEncoding("UTF-8");

		PrintWriter out = resp.getWriter();

		out.println("<html lang=\"pl\">");
		out.println("<head></head>");
		out.println("<body>");

		out.println("<h1>Kalkulator</h1>");
		out.println("<form action=\"/calculator\" method=\"get\">");

		// pobieramy wartosc a
		// jesli jest podana, to ona ma byc selected
		// jesli nie jest podana, to niech selected bedzie 0
		// do metody przekazujemy zmienna (int) ktora ma byc selected

		printNumbersForValue("a", req, resp);

		printOperations(out, req);

		printNumbersForValue("b", req, resp);

		out.println("<input type=\"submit\" value=\"oblicz\">");
		out.println("</form>");

		// TODO: te metode implementujemy
		printResult(req, resp);

		out.println("</body>");
		out.println("</html>");
	}

	private void printResult(HttpServletRequest req, HttpServletResponse resp) throws IOException {

		PrintWriter out = resp.getWriter();

		// pobieramy wartosc parametrow a, b i o
		// http://localhost:8080/calculator?a=5&o=plus&b=8
		String aStr = req.getParameter("a");
		String bStr = req.getParameter("b");
		String oStr = req.getParameter("o");

		// uzupelniamy warunek
		// jesli aStr, bStr i oStr nie sa (wszystkie) null
		if (aStr != null && !aStr.isEmpty() && bStr != null && !bStr.isEmpty()
				&& oStr != null && !oStr.isEmpty()) {
			int a = Integer.parseInt(aStr);
			int b = Integer.parseInt(bStr);
			// zmienna na wynik
			int result = 0;

			out.println("<br>");
			out.println("<h4>Wynik</h4>");

			// wyswietlamy wartosc a
			out.println(a);
			if ("plus".equals(oStr)) {
				out.println(" + ");
				result = a + b;
			} else if ("minus".equals(oStr)) {
				out.println(" - ");
				result = a - b;
			} else if ("multi".equals(oStr)) {
				out.println(" * ");
				result = a * b;
			} else if ("divide".equals(oStr)) {
				out.println(" / ");
				result = a / b;
			} else if ("modulo".equals(oStr)) {
				out.println(" % ");
				result = a % b;
			}

			// wyswietlamy wartosc b
			out.println(b);
			out.println(" = ");

			// wyswietlamy wynik
			out.println(result);
		}
	}

	private void printOperations(PrintWriter out, HttpServletRequest req) {
		String o = req.getParameter("o");
		String valueToSelect = "plus";
		if (o != null && !o.isEmpty()) {
			valueToSelect = o;
		}

		Map<String, String> operators = new HashMap<>();
		operators.put("plus", " + ");
		operators.put("minus", " - ");
		operators.put("multi", " * ");
		operators.put("divide", " / ");
		operators.put("modulo", " % ");

		out.println("Wybierz operator: ");

		out.println("<select name=\"o\">");
		for (Map.Entry<String, String> entry : operators.entrySet()) {
			if (valueToSelect.equals(entry.getKey())) {
				out.println("<option value=\"" + entry.getKey() + "\" selected>" + entry.getValue() + "</option>");
			} else {
				out.println("<option value=\"" + entry.getKey() + "\">" + entry.getValue() + "</option>");
			}
		}
		out.println("</select>");
		out.println("<br/>");

		// ?o=multi
	}

	private void printNumbersForValue(String value, HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String parameter = req.getParameter(value);
		System.out.println(parameter);

		int valueToSelect = 0;
		if (parameter != null && !parameter.isEmpty()) {
			valueToSelect = Integer.parseInt(parameter);
		}

		PrintWriter out = resp.getWriter();
		out.println("Wybierz wartość " + value + ": ");
		out.println("<select name=\"" + value + "\">");
		for (int i = MIN; i <= MAX; i++) {
			if (i == valueToSelect) {
				out.println("<option value=\"" + i + "\" selected>" + i + "</option>");
			} else {
				out.println("<option value=\"" + i + "\">" + i + "</option>");
			}
		}

		out.println("</select>");
		out.println("<br/>");
	}

}
