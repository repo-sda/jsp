package pl.sda.jsp;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter(filterName = "profiler", urlPatterns = {"/profiler"})
public class ProfilerFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
	                     FilterChain chain) throws IOException, ServletException {

		long start = System.currentTimeMillis();
		PrintWriter out = response.getWriter();
		//out.println("przed");

		chain.doFilter(request, response);

		long end = System.currentTimeMillis();
		System.out.println("czas: " + (end - start));
		//out.println("po filtrze");
	}

	@Override
	public void destroy() {

	}
}
