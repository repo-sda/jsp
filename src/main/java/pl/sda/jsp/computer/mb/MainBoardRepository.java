package pl.sda.jsp.computer.mb;

import pl.sda.jsp.computer.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class MainBoardRepository implements Repository<MainBoard> {

	private static final Map<Integer, MainBoard> items = new HashMap<>();

	@Override
	public MainBoard getById(int id) {
		return items.get(id);
	}

	@Override
	public Collection<MainBoard> getAll() {
		return items.values();
	}

	static {
		// TODO: mozecie rozszerzyc liste
		items.put(1, new MainBoard(1, "Gigabyte TX 400", 800));
		items.put(2, new MainBoard(2, "Gigabyte TX 600", 1100));
		items.put(3, new MainBoard(3, "Seagate AX 1200", 1200));
		items.put(4, new MainBoard(4, "Seagate AX 1500", 1600));
		items.put(5, new MainBoard(5, "Seagate AX 2000", 2100));

	}
}