package pl.sda.jsp.computer;

import java.util.Collection;

public interface Repository<T> {

	T getById(int id);
	Collection<T> getAll();
}
