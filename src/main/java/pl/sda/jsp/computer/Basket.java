package pl.sda.jsp.computer;

import java.util.Collection;
import java.util.stream.Collectors;

public class Basket {

	private final Collection<Item> items;
	private final int totalPrice;

	public Basket(Collection<Item> items) {
		this.items = items;
		this.totalPrice = items.stream().collect(Collectors.summingInt(Item::getPrice));
	}

	public Collection<Item> getItems() {
		return items;
	}

	public int getTotalPrice() {
		return totalPrice;
	}
}
