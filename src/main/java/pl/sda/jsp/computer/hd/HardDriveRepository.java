package pl.sda.jsp.computer.hd;

import pl.sda.jsp.computer.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class HardDriveRepository implements Repository<HardDrive> {

	private static final Map<Integer, HardDrive> items = new HashMap<>();

	@Override
	public HardDrive getById(int id) {
		return items.get(id);
	}

	@Override
	public Collection<HardDrive> getAll() {
		return items.values();
	}

	static {
		// TODO: mozecie rozszerzyc liste
		items.put(1, new HardDrive(1, "Seagate 500 GB", 100));
		items.put(2, new HardDrive(2, "Seagate 1 TB", 200));
		items.put(3, new HardDrive(3, "Seagate 2 TB", 300));
		items.put(4, new HardDrive(4, "Seagate 4 TB", 500));
	}
}
