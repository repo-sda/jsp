package pl.sda.jsp.computer.proc;

import pl.sda.jsp.computer.Item;

public class Processor implements Item {

	private final int id;
	private final String name;
	private final int price;

	public Processor(int id, String name, int price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getPrice() {
		return price;
	}

	@Override
	public String toString() {
		return name + "(" + price + ")";
	}
}
