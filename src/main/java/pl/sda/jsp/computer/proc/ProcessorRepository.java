package pl.sda.jsp.computer.proc;

import pl.sda.jsp.computer.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class ProcessorRepository implements Repository<Processor> {

	private static final Map<Integer, Processor> items = new HashMap<>();

	@Override
	public Processor getById(int id) {
		return items.get(id);
	}

	@Override
	public Collection<Processor> getAll() {
		return items.values();
	}

	static {
		// TODO: mozecie rozszerzyc liste
		items.put(1, new Processor(1, "Intel i5 2 Core", 500));
		items.put(2, new Processor(2, "Intel i5 4 Core", 700));
		items.put(3, new Processor(3, "Intel i7 4 Core", 900));
		items.put(4, new Processor(4, "Intel i7 6 Core", 1200));
		items.put(5, new Processor(5, "Intel i7 8 Core", 1800));
	}
}
