package pl.sda.jsp.computer;

import pl.sda.jsp.computer.hd.HardDrive;
import pl.sda.jsp.computer.hd.HardDriveRepository;
import pl.sda.jsp.computer.mb.MainBoardRepository;
import pl.sda.jsp.computer.proc.Processor;
import pl.sda.jsp.computer.proc.ProcessorRepository;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

@WebServlet(value = "/computer")
public class ComputerServlet extends HttpServlet {

	private Repository hardDriveRepository = new HardDriveRepository();
	private Repository processorRepository = new ProcessorRepository();
	private Repository mainBoardRepository = new MainBoardRepository();

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		// udostepniam dane: wszystkie dyski, procesory
		req.setAttribute("hds", hardDriveRepository.getAll());
		req.setAttribute("procs", processorRepository.getAll());
		req.setAttribute("mbs", mainBoardRepository.getAll());

		req.getRequestDispatcher("computer.jsp").include(req, resp);
	}
}
