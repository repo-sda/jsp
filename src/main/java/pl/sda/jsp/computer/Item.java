package pl.sda.jsp.computer;

public interface Item {

	int getId();
	String getName();
	int getPrice();
}
