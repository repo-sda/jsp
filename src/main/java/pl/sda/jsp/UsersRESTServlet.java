package pl.sda.jsp;

import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "Users", value = "/users")
public class UsersRESTServlet extends HttpServlet {

	private Map<Integer, User> users;

	@Override
	public void init() throws ServletException {
		users = new HashMap<>();
		users.put(1, new User(1, "Ala", 23));
		users.put(2, new User(2, "Basia", 18));
		users.put(3, new User(3, "Karol", 34));
		users.put(4, new User(4, "Rafał", 40));
		users.put(5, new User(5, "Tomek", 25));
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();

		resp.setContentType("application/json");

		// /users?id=2 (istniejacy) to wyswietla usera o tym id
		// /users?id=27 (NIEistniejacy) to wyswietla komunikat, ze user o id 27 nie istnieje

		String idAsString = req.getParameter("id");
		if (idAsString != null && !idAsString.isEmpty()) {
			int id = Integer.valueOf(idAsString);
			Gson gson = new Gson();
			User user = users.get(id);
			if (user != null) {
				out.println(gson.toJson(user));
			} else {
				out.println(gson.toJson("User #" + id + " nie istnieje!"));
			}
		}
	}
}
