<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Numbers</title>
    <link media="all" type="text/css" href="/s.css" rel="stylesheet">
</head>
<body>

<%@ include file="header.jspf"%>

<h1>Numbers</h1>

<%  // domyslnie
    int max = 10;
    String maxParam = request.getParameter("max");
    if (maxParam != null && !maxParam.isEmpty()) {
        int maxParamAsInt = Integer.parseInt(maxParam);
        // musi byc wieszy od zera
        if (maxParamAsInt > 0) {
        	max = maxParamAsInt;
        }
    }
%>

<table border="1" cellpadding="1" cellspacing="2">
    <tr>
        <th>liczba</th><th>kwadrat</th>
    </tr>

<% for (int i=1; i<=max; i++) { %>
    <tr <%=(i%2==0 ? "bgcolor=green": "")%>>
        <td><%=i%></td><td><%=i*i%></td>
    </tr>
<% } %>

</table>


</body>
</html>
