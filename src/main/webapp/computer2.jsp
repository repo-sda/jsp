<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Computer</title>
</head>
<h1>Złóż swój własny komputer</h1>

<form>
    Wybierz procesor:
    <select name="proc">
        <c:forEach items="${procs}" var="item">
            <option value="${item.id}">${item.name} (${item.price})</option>
        </c:forEach>
    </select>
    <br/>

    Wybierz dysk twardy:
    <select name="hd">
        <c:forEach items="${hds}" var="item">
            <option value="${item.id}">${item.name} (${item.price})</option>
        </c:forEach>
    </select>
    <br/>

    <input type="submit" value="kupuję">

</form>

<c:if test="${not empty basket}">
    <h2>Wybrana konfiguracja</h2>
    <c:forEach items="${basket.items}" var="item">
        ${item} <br>
    </c:forEach>
    <br>
    Kwota total: <b>${basket.totalPrice}zł</b>
    <br>
    <br>
    <a href="/computer">utwórz nową konfigurację</a>
</c:if>

</body>
</html>
