<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<%@ page import="pl.sda.jsp.computer.proc.Processor" %>
<%@ page import="java.util.Collection" %>
<%@ page import="pl.sda.jsp.computer.hd.HardDrive" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Computer</title>
    <%-- <meta charset="utf-8">--%>
</head>
<body>
<%
    Collection<Processor> processors = (Collection<Processor>) request.getAttribute(
            "procs");
    Collection<HardDrive> hardDrives = (Collection<HardDrive>) request.getAttribute(
            "hds");
%>
<h1>Zloz swoj wlasny komputer</h1>
<form>
    Wybierz procesor:
    <select name="proc">
        <%
            for (Processor i : processors) {
        %>
        <option value="<%=i.getId()%>"><%=i.getName()%> (<%=i.getPrice()%>zl)</option>
        <%
            }
        %>
    </select>
    <br/>
    Wybierz dysk twardy:
    <select name="hd">
        <%
            for (HardDrive i : hardDrives) {
        %>
        <option value="<%=i.getId()%>"><%=i.getName()%> (<%=i.getPrice()%>zl)</option>
        <%
            }
        %>
    </select>
    <br/>
    <input type="submit" value="kupuje">
</form>
</body>
</html>
