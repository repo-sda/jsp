<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html lang="pl">
<head>
    <title>Computer</title>
</head>
<body>
<h1>Złóż swój własny komputer</h1>

<form name="comp">
    Wybierz procesor:
    <select name="proc">
        <c:forEach items="${procs}" var="item">
            <option value="${item.id}">${item.name} (${item.price})</option>
        </c:forEach>
    </select>
    <br>

    Wybierz dysk twardy:
    <select name="hd">
        <c:forEach items="${hds}" var="item">
            <option value="${item.id}">${item.name} (${item.price})</option>
        </c:forEach>
    </select>
    <br>

    Wybierz płytę główną:
    <select name="mbs">
        <c:forEach items="${mbs}" var="item">
            <option value="${item.id}">${item.name} (${item.price})</option>
        </c:forEach>
    </select>
    <br>

    <input type="submit" value="kupuje">
</form>

</body>
</html>